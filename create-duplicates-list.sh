#!/bin/bash

# Xavier Belanger
# December  8, 2024

# this script is covered by the BSD 2-Clause License

# cheking if an argument is present
if [ $# -lt 1 ]
then
	/bin/printf "usage: $0 directory\n"
	exit 1
fi

# checking if the argument is a directory
if [ ! -d $1 ]
then
	/bin/printf "Bad argument\n"
	exit 1
fi

# create a list of duplicate files, based on their MD5 checksum,
# with minimum permissions (file names could be sensitive)
umask 0077
OUTPUT=/tmp
CSVFILE=$OUTPUT/duplicate-files-list-$(/bin/date +"%F-%H-%M").csv

# we need a few temporary files
for TMPFILE in TMPFILE1 TMPFILE2 TMPFILE3 TMPFILE4 TMPFILE5 TMPFILE6 TMPFILE7 TMPFILE8 TMPFILE9 TMPFILE10
do
	eval $TMPFILE=$(/usr/bin/mktemp $OUTPUT/create-duplicates-list.XXXXXX)
done

# get the full name of the source directory
directory=$(/usr/bin/readlink -f $1)

# get the size for each file (once for sort, two for uniq)
/bin/printf "[ $(/bin/date +"%F %T") ] Listing file sizes...\n"
/usr/bin/find $directory -type f -printf '%s;%h/%f;%s\n' > $TMPFILE1
/usr/bin/sort -n $TMPFILE1 > $TMPFILE2

# replacing filenames' spaces for 'uniq' to skip fields properly
/usr/bin/sed -i 's/ /|/g' $TMPFILE2
/usr/bin/sed -i 's/;/ /g' $TMPFILE2
/usr/bin/uniq -D --skip-field=2 $TMPFILE2 | /usr/bin/cut -d " " -f2 > $TMPFILE3
# and revert the change
/usr/bin/sed -i 's/|/ /g' $TMPFILE3

# adding the initial directory for each listed file
# you must use another separator with sed because of the slash in the folder name
# plus double quotes for the variable itself
/usr/bin/sed -i "s|^\.|$directory|" $TMPFILE3

# create the MD5 checksum for each listed file
/bin/printf "[ $(/bin/date +"%F %T") ] Computing MD5 checksums...\n"
/usr/bin/cat $TMPFILE3 | while read listedFile ; do md5sum "$listedFile" >> $TMPFILE4 ; done
/bin/printf "[ $(/bin/date +"%F %T") ] Done.\n"

# sort the list
/bin/printf "[ $(/bin/date +"%F %T") ] Sorting the list...\n"
/bin/sort $TMPFILE4 > $TMPFILE5
/bin/printf "[ $(/bin/date +"%F %T") ] Done.\n"

# eliminate non-duplicated checksums
/bin/printf "[ $(/bin/date +"%F %T") ] Listing duplicates...\n"
/bin/uniq -D -w 32 $TMPFILE5 > $TMPFILE6
/bin/printf "[ $(/bin/date +"%F %T") ] Done.\n"

# stops the script if there is no duplicate
if [ ! -s $TMPFILE6 ]
then
	/bin/printf "[ $(/bin/date +"%F %T") ] No duplicated files found.\n"

	# deleting temporary files
	/bin/rm $TMPFILE1 $TMPFILE2 $TMPFILE3 $TMPFILE4 $TMPFILE5 $TMPFILE6 $TMPFILE7 $TMPFILE8 $TMPFILE9 $TMPFILE10

	exit
fi

/bin/printf "[ $(/bin/date +"%F %T") ] Generating $CSVFILE...\n"
# set a colon as a separator between the checksum and the file name
/bin/sed 's/  /:/' $TMPFILE6 > $TMPFILE7

# split each line in three different fields
# * checksum
/bin/cut -d":" -f1 $TMPFILE7 > $TMPFILE8

# * directory name
/bin/cut -d":" -f2 $TMPFILE7 | while read filename ; do /bin/dirname "$filename" ; done > $TMPFILE9

# * file name
/bin/cut -d":" -f2 $TMPFILE7 | while read filename ; do /bin/basename "$filename" ; done > $TMPFILE10

# paste all intermediate files together
/bin/paste -d: $TMPFILE8 $TMPFILE9 $TMPFILE10 > $CSVFILE

# replace the colons by a comma with double quotes
/bin/sed -i 's/:/","/g' $CSVFILE

# add a double quote at the beginning of the line
/bin/sed -i 's/^/"/' $CSVFILE

# add a double quote at the end of the line
/bin/sed -i 's/$/"/' $CSVFILE

# add a header on the first line
/bin/sed -i 1i"\"CHECKSUM\",\"DIRECTORY\",\"FILE\"" $CSVFILE
/bin/printf "[ $(/bin/date +"%F %T") ] Done.\n"

# deleting temporary files
/bin/rm $TMPFILE1 $TMPFILE2 $TMPFILE3 $TMPFILE4 $TMPFILE5 $TMPFILE6 $TMPFILE7 $TMPFILE8 $TMPFILE9 $TMPFILE10

# EoF
