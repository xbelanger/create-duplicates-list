# create-duplicates-list

This script will generate a list of duplicated files in a directory, based on their MD5 checksum.

You will need to provide the name of the directory as an argument for the script:

`create-duplicates-list directory`

The output is a file named with a timestamp: `/tmp/duplicate-files-list-YYYY-MM-DD-HH-MM.csv`
